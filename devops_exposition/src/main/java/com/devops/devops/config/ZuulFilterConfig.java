package com.devops.devops.config;

import com.devops.devops.filters.JwtFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by cdepadua on 09/02/2018.
 */
@Configuration
public class ZuulFilterConfig {

    /**
     * Filter for Pres/Bis security.
     * @return JwtFilter
     */
    @Bean
    public JwtFilter jwtFilter() {
        return new JwtFilter();
    }

}
