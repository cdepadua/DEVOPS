package com.devops.devops.security;

import com.devops.devops.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

/**
 * Created by cdepadua on 09/02/2018.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    private static String REALM_NAME ="RESTFUL_REALM";

    @Autowired
    private AuthService authService;

    /**
     * @param auth
     * @throws Exception
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers   ("/api/devops/user-ms/user/signup",
                        "/api/devops/user-ms/user/login",
                        "/api/devops/user-ms/user/get_user_info",
                        "/api/devops/hotel-ms/hotel/find_all_hotels",
                        "/api/devops/hotel-ms/hotel/get_rooms_by_cd_hotel/**",
                        "/api/devops/hotel-ms/hotel/get_options_by_cd_hotel/**",
                        "/api/devops/hotel-ms/hotel/get_hotel_by_*/**").permitAll()
                .antMatchers("/api/devops/user-ms/user/adminauth/**").hasAnyAuthority("ADMIN")
                .antMatchers("/api/devops/user-ms/user/userauth/**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/devops/hotel-ms/option/adminauth/**").hasAnyAuthority("ADMIN")
                .antMatchers("/api/devops/hotel-ms/option/userauth/**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/devops/hotel-ms/room/adminauth/**").hasAnyAuthority("ADMIN")
                .antMatchers("/api/devops/hotel-ms/room/userauth/**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/devops/hotel-ms/hotel/adminauth/**").hasAnyAuthority("ADMIN")
                .antMatchers("/api/devops/hotel-ms/hotel/userauth/**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/devops/reservation-ms/reservation/adminauth/**").hasAnyAuthority("ADMIN")
                .antMatchers("/api/devops/reservation-ms/reservation/userauth/**").hasAnyAuthority("ADMIN", "USER")
                .anyRequest().authenticated()
                .and().httpBasic()
                .realmName(REALM_NAME).authenticationEntryPoint(getBasicAuthEntryPoint())
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }


    /**
     * @return basicAuthEntryPoint;
     */
    @Bean
    public BasicAuthenticationEntryPoint getBasicAuthEntryPoint(){
        BasicAuthenticationEntryPoint basicAuthEntryPoint = new BasicAuthenticationEntryPoint();
        basicAuthEntryPoint.setRealmName(REALM_NAME);
        return basicAuthEntryPoint;
    }
}
