package com.devops.devops.auth;


import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by cdepadua on 09/02/2018.
 */
public interface AuthService extends UserDetailsService {
    /** {@IneritDoc} */
    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
