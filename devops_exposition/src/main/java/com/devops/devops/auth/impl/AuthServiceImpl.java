package com.devops.devops.auth.impl;

import com.devops.devops.auth.AuthService;
import com.devops.devops.dto.UserDto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by cdepadua on 09/02/2018.
 */
@Service
public class AuthServiceImpl implements AuthService {

    @Value("${back.api.uri}")
    String uri;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        RestTemplate restTemplate = new RestTemplate(); 
        UserDto user = restTemplate.postForObject(uri + "/user-ms/user/get_user_info", username, UserDto.class);
        return new User(username, user.getPassword(), true, true, true,
				true, AuthorityUtils.createAuthorityList(user.getRole()));
    }
}
