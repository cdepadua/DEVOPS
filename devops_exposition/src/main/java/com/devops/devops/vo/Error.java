package com.devops.devops.vo;

import java.io.Serializable;

/**
 * Created by cdepadua on 09/02/2018.
 */
public class Error implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * code.
     */
    private final String code;
    /**
     * description.
     */
    private final String description;
    /**
     * data.
     */
    private final String[] data;
    /**
     * fatal, true / false.
     */
    private final boolean fatal;


    /**
     * Build by RuntimeException.
     * @param e run time exception.
     */
    public Error(final RuntimeException e) {
        super();
        this.code = "TECHNICAL";
        this.description = e.getMessage();
        this.data = null;
        this.fatal = true;
    }

    /**
     * Build by Exception.
     * @param e exception.
     */
    public Error(final Exception e) {
        super();
        this.code = "TECHNICAL";
        this.description = e.getMessage();
        this.data = null;
        this.fatal = true;
    }

    /**
     * Return the error code.
     * @return code d'erreur.
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Return data.
     * @return datas.
     */
    public String[] getData() {
        if (this.data == null) {
            return new String[0];
        }
        return this.data;
    }

    /**
     * Return error's message.
     * @return message d'erreur.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * True if the error is fatal, false else.
     * @return fatal, true /false
     */
    public boolean isFatal() {
        return this.fatal;
    }

}
