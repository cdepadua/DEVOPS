package com.devops.devops.filters;

import com.devops.devops.vo.Error;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.text.MessageFormat;
import java.util.Objects;
import io.jsonwebtoken.Jwts;


/**
 * Created by cdepadua on 09/02/2018.
 */
public class JwtFilter extends ZuulFilter {

    /**
     * constante pour l'ordre du filtre.
     */
    private static final int FILTER_ORDER = 9;

    /**
     * jwtHeader.
     */
    @Value("${gateway.security.jwt.header}")
    private String jwtHeader;
    /**
     * jwtSecret.
     */
    @Value("${gateway.security.jwt.secret}")
    private String jwtSecret;

    /**
     * {@link Logger}. {@link JwtFilter}
     */
    private static Logger log = LoggerFactory.getLogger(JwtFilter.class);

    /**
     * type de filtre.
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * ordre du filtre. constante.
     */
    @Override
    public int filterOrder() {
        return FILTER_ORDER;
    }

    /**
     * shouldFilter, vrai ou faux.
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * run.
     */
    @Override
    public Object run() {
        // Initialisations
        final RequestContext ctx = RequestContext.getCurrentContext();

        final HttpServletRequest request = ctx.getRequest();
        final Principal principal = request.getUserPrincipal();
        String uid = "ANONIMUS";
        
        if(principal != null) {
        	uid = principal.getName();
        }
        //Objects.requireNonNull(principal);
        final String ipAddress = request.getHeader("ipAddress");

        // Trace request
        log.debug(
                MessageFormat.format("{0} request to {1}", request.getMethod(), request.getRequestURL().toString()));



        // Build JWT token and put in request's header
        final String jwtToken = Jwts.builder().setSubject(uid)
                .claim("ipAddress", ipAddress).signWith(SignatureAlgorithm.HS512, this.jwtSecret).compact();
        ctx.addZuulRequestHeader(this.jwtHeader, jwtToken);
        return null;
    }

    /**
     * Send error and block request.
     * @param ctx ctx
     * @param description description
     * @return {@link Object} erreur
     */
    private Object error(final RequestContext ctx, final String description) {
        log.error(description);
        final Exception e = new Exception(description);
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final String json =
                    mapper.writeValueAsString(new Error(e));
            ctx.setSendZuulResponse(false);
            ctx.setResponseBody(json);
            ctx.setResponseStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        catch (final JsonProcessingException e1) {
            log.error("Serialisation error", e1);
        }
        return null;
    }

    /**
     * Build authority from profile.
     * @param profile profile
     * @return {@link GrantedAuthority}
     */
    private GrantedAuthority buildAuthority(final String profile) {
        return new SimpleGrantedAuthority(profile);
    }
}