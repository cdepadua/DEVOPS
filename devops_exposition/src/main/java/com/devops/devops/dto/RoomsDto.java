package com.devops.devops.dto;

/**
 * @author cesar
 * Room dto
 */
public class RoomsDto {
	
	/** Room Id. */
	private long Id;
	
	/** Type de chambre. */
	private String type;
	
	/** Number of room. */
	private Integer nbOfRoom; 
	
	/**
	 * @return the id
	 */
	public long getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		Id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(final String type) {
		this.type = type;
	}

	/**
	 * @return the nbOfRoom
	 */
	public Integer getNbOfRoom() {
		return nbOfRoom;
	}

	/**
	 * @param nbOfRoom the nbOfRoom to set
	 */
	public void setNbOfRoom(final Integer nbOfRoom) {
		this.nbOfRoom = nbOfRoom;
	}	
	
}
