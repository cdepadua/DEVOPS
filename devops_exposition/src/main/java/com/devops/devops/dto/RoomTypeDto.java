package com.devops.devops.dto;


/**
 * @author cesar
 * RoomTypeDto
 */
public class RoomTypeDto {
	/** Id room. */
	private long id;
	
	/** Label. */
	String label;
	
	/** Room code. */
	String cdRoom;
	
	/** Maximal number of people. */
	Integer nbOfPeople;
	
	/** Room price. */
	Integer price;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * @return the cdRoom
	 */
	public String getCdRoom() {
		return cdRoom;
	}

	/**
	 * @param cdRoom the cdRoom to set
	 */
	public void setCdRoom(final String cdRoom) {
		this.cdRoom = cdRoom;
	}

	/**
	 * @return the nbOfPeople
	 */
	public Integer getNbOfPeople() {
		return nbOfPeople;
	}

	/**
	 * @param nbOfPeople the nbOfPeople to set
	 */
	public void setNbOfPeople(final Integer nbOfPeople) {
		this.nbOfPeople = nbOfPeople;
	}

	/**
	 * @return the price
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(final Integer price) {
		this.price = price;
	}
	
}
