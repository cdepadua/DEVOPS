package com.devops.devops;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author cesar
 *
 */
@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest
public class DevopsApplicationTests {

	/**
	 * 
	 */
	@Test
	public void contextLoads() {
	}

}
