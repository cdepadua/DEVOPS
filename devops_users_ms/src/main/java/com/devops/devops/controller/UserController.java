package com.devops.devops.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import com.devops.devops.converter.UserDtoConverter;
import com.devops.devops.dto.UserDto;
import com.devops.devops.service.IUserService;

/** UserController. */
@RestController
@RequestMapping("/user-ms/user")
public class UserController {
	
	/** UserService. */
	@Autowired
	IUserService userService;
	
	/**
	 * Create a new user
	 * @param myUserDto
	 * @return created user
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public final UserDto createUser(@RequestBody final UserDto myUserDto) {
		return UserDtoConverter.toDTO(this.userService
				.createUser(UserDtoConverter.toBIZ(myUserDto)));
	}

	@RequestMapping(value = "/get_user_info", method = RequestMethod.POST)
	public final UserDto getUserInfo(@RequestBody final String username) throws UsernameNotFoundException {
		UserDetails ud = this.userService.loadUserByUsername(username);
		UserDto uDto = new UserDto();
		uDto.setEmail(ud.getUsername());
		uDto.setPassword(ud.getPassword());
		uDto.setRole(ud.getAuthorities().toString().replace("[", "").replace("]",""));
		return uDto;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public final UserDetails login(@RequestBody final UserDto userDto) {
		return this.userService.login(UserDtoConverter.toBIZ(userDto));
	}
}
