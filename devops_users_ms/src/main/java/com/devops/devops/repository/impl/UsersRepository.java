package com.devops.devops.repository.impl;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.devops.devops.model.Users;
import com.devops.devops.repository.AbstractRepository;
import com.devops.devops.repository.IUsersRepository;

/**
 * @author cesar
 *	User repository implementation
 */
@Repository
public class UsersRepository extends AbstractRepository implements IUsersRepository {
	
	/** {@IneritDoc} */
	@Override
	public Users createUser(Users myUser){
		this.getEm().persist(myUser);
		this.getEm().flush();
		return myUser;
	}
	
	/** {@IneritDoc} */
	@Override
	public Users getUserByUsername(String email) throws NoResultException{
		Users user = null;
		try {
			user = (Users) this.getEm().createNativeQuery("SELECT * FROM users u WHERE u.email = '" + email + "'", Users.class)
					.getSingleResult();
		} catch (NoResultException noresult) {
			throw new NoResultException();
		}
		return user;
	}

	@Override
	public Users login(Users user) throws NoResultException {
		Users userToReturn = null;
		try {
			userToReturn = (Users) this.getEm().createNativeQuery("SELECT * FROM users u WHERE u.email = '" + user.getEmail() + 
					"' AND u.password = '"+user.getPassword()+"'", Users.class)
					.getSingleResult();
		} catch (NoResultException noresult) {
			throw new NoResultException();
		}
		return userToReturn;
	}
}
