package com.devops.devops.repository;

import javax.persistence.NoResultException;

import com.devops.devops.model.Users;

/**
 * @author cesar
 * Interface user repository
 */
public interface IUsersRepository {
	/**
	 * Create a new user
	 * @param user
	 * @return user
	 */
	Users createUser(Users user);
	
	/**
	 * find user by email
	 * @param email user mail
	 * @return user
	 * @throws NoResultException 
	 */
	Users getUserByUsername(String email)throws NoResultException;
	
	/**
	 * login
	 * @param user
	 * @return
	 * @throws NoResultException
	 */
	Users login(Users user) throws NoResultException;
}
