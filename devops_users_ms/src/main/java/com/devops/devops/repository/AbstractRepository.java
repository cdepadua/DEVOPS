package com.devops.devops.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/** AbstractRepository. */
public abstract class AbstractRepository {
	/** EntityManager. */
	@PersistenceContext
	private EntityManager em;

	/**
	 * @return the em
	 */
	public EntityManager getEm() {
		return em;
	}

	/**
	 * @param em the em to set
	 */
	public void setEm(final EntityManager em) {
		this.em = em;
	}
	
	
}
