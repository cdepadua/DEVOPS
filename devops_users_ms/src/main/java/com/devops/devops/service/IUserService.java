package com.devops.devops.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import com.devops.devops.model.Users;

/** UserService Inteface. */
public interface IUserService extends UserDetailsService {
	/**Create a new user
	 * @param user
	 * @return created user
	 */
	Users createUser(final Users user);
	/**
	 * login
	 * @param user
	 * @return
	 */
	UserDetails login(Users user);
}
