package com.devops.devops.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devops.devops.model.Users;
import com.devops.devops.repository.IUsersRepository;
import com.devops.devops.service.IUserService;

/**
 * @author cesar
 *	user service
 */
@Service
@Transactional(rollbackFor = {RuntimeException.class})
public class UserService implements IUserService {
	
	@Autowired
	IUsersRepository userRepository;
	/** {@IneritDoc} */
	@Override
	public Users createUser(final Users user){
		return this.userRepository.createUser(user);
	}
	@Override
	public UserDetails login(Users user) {
		Users userToReturn = this.userRepository.login(user);
		return new User(userToReturn.getEmail(), user.getPassword(), true, true, true,
				true, AuthorityUtils.createAuthorityList(user.getRole()));
	}
	/** {@IneritDoc} */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Users user = this.userRepository.getUserByUsername(username);
		return new User(username, user.getPassword(), true, true, true,
				true, AuthorityUtils.createAuthorityList(user.getRole()));
	}
	
}
