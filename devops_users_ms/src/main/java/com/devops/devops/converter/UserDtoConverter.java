package com.devops.devops.converter;

import com.devops.devops.dto.UserDto;
import com.devops.devops.model.Users;

/**
 * @author cesar
 * UserDto converter
 */
public final class UserDtoConverter {
	/** Constructor. */
	private UserDtoConverter(){}
	
	/**
	 * Convert User entity to user dto
	 * @param user
	 * @return user dto
	 */
	public static UserDto toDTO(Users user) {
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setFirstname(user.getFirstname());
		userDto.setLastname(user.getLastname());
		userDto.setPassword(user.getPassword());
		userDto.setPhoneNumber(user.getPhoneNumber());
		userDto.setEmail(user.getEmail());
		userDto.setRole(user.getRole());
		return userDto;
	}
	
	/**
	 * Convert user dto to user entity
	 * @param userDto
	 * @return user dto
	 */
	public static Users toBIZ(UserDto userDto) {
		Users user = new Users();
		user.setId(userDto.getId());
		user.setFirstname(userDto.getFirstname());
		user.setLastname(userDto.getLastname());
		user.setPassword(userDto.getPassword());
		user.setPhoneNumber(userDto.getPhoneNumber());
		user.setEmail(userDto.getEmail());
		if(userDto.getRole() == null)
			user.setRole("USER");
		else	
			user.setRole(userDto.getRole());
		return user;
	}
}
