package com.devops.devops.service;

import com.devops.devops.dto.ReservationDto;
import com.devops.devops.model.Reservation;

/**
 * @author cesar
 * Interface reservation service
 */
public interface IReservationService {
	/**
	 * create a reservation
	 * @param reservation
	 * @return created reservation
	 */
	Reservation createReservation(ReservationDto reservation);
}
