package com.devops.devops.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.devops.devops.converter.ReservationDtoConverter;
import com.devops.devops.dto.OptionForReservationDto;
import com.devops.devops.dto.OptionsDto;
import com.devops.devops.dto.ReservationDto;
import com.devops.devops.dto.RoomForReservationDto;
import com.devops.devops.dto.RoomsDto;
import com.devops.devops.model.OptionReservation;
import com.devops.devops.model.Reservation;
import com.devops.devops.model.RoomReservation;
import com.devops.devops.repository.IOptionReservationRepository;
import com.devops.devops.repository.IReservationRepository;
import com.devops.devops.repository.IRoomReservationRepository;
import com.devops.devops.service.IReservationService;


/**
 * @author cesar
 * Reservation Service
 */
@Service
@Transactional(rollbackFor = {RuntimeException.class})
public class ReservationService implements IReservationService{
	
	@Value("${back.api.uri}")
    String uri;
	
	/** Reservation repository. */
	@Autowired
	private IReservationRepository reservationRepository;
	
	@Autowired
	private IRoomReservationRepository roomReservationRepository;
	
	@Autowired
	private IOptionReservationRepository optionReservationRepository;

	@SuppressWarnings("unchecked")
	@Override
	public Reservation createReservation(ReservationDto reservation) {
		
		Reservation res = ReservationDtoConverter.toBIZ(reservation);
		
		RestTemplate restTemplate = new RestTemplate();

		/* Data for room reservations*/
		List<RoomReservation> notEmptyRooms = this.roomReservationRepository
				.getNotEmptyRoomByDate(reservation.getFromDate(), reservation.getToDate(), reservation.getCdHotel());
		
		ResponseEntity<RoomForReservationDto[]>allRoomsArr = restTemplate.getForEntity(uri + "/hotel-ms/hotel/get_rooms_by_cd_hotel/" 
				+ reservation.getCdHotel(), RoomForReservationDto[].class);
		List<RoomForReservationDto> allRooms = Arrays.asList(allRoomsArr.getBody());
		
		/* Data for option reservation */
		List<OptionReservation> notEmptyOptions = this.optionReservationRepository
				.getNotEmptyOptionByDate(reservation.getFromDate(), reservation.getToDate(), reservation.getCdHotel());
		
		ResponseEntity<OptionForReservationDto[]> allOptionsArr = restTemplate.getForEntity(uri +  "/hotel-ms/hotel/get_options_by_cd_hotel/"
				+ reservation.getCdHotel(), OptionForReservationDto[].class);
		List<OptionForReservationDto> allOptions = Arrays.asList(allOptionsArr.getBody());
		
		Integer price = 0;
	
		List<RoomReservation> roomReservationsList = new ArrayList<>();
		for(RoomsDto room : reservation.getListRoom()){
			Long roomId = null;
			for(int i = 0; i < room.getNbOfRoom(); i++){
				for(RoomForReservationDto roomDto : allRooms){
					if(roomDto.getType().equals(room.getType())) {
						Boolean empty = false;
						for(RoomReservation roomRes : notEmptyRooms) {
							if(roomRes.getId().equals(roomDto.getId())){
								empty = true;
							}
						}
						if(!empty) {
							roomId = roomDto.getId();
							price += roomDto.getPrice();
						}
					}
				}
				RoomReservation roomReservation = new RoomReservation();
				roomReservation.setCdHotel(reservation.getCdHotel());
				roomReservation.setIdRoom(roomId);
				roomReservation.setReservation(res);
				roomReservationsList.add(roomReservation);
			}
		}
		res.setRoomReservationList(roomReservationsList);
		
		List<OptionReservation> optionReservationsList = new ArrayList<>();
		for(OptionsDto option : reservation.getListOption()){
			Long optionId = null;
			for(int i = 0; i < option.getNumber(); i++) {
				for(OptionForReservationDto optionDto : allOptions) {
					if(optionDto.getType().equals(option.getOptionName())) {
						Boolean empty = false;
						for(OptionReservation optionRes : notEmptyOptions) {
							if(optionRes.getId().equals(optionDto.getId())) {
								empty = true;
							}
						}
						if(!empty) {
							optionId = optionDto.getId();
							price += optionDto.getPrice();
						}
					}
				}
				OptionReservation optionReservation = new OptionReservation();
				optionReservation.setCdHotel(reservation.getCdHotel());
				optionReservation.setIdOption(optionId);
				optionReservation.setReservation(res);
				optionReservationsList.add(optionReservation);
			}
		}
		res.setOptionReservationList(optionReservationsList);
		
		res.setTotalPrice(price);

		return this.reservationRepository.createReservation(res);
	}

}
