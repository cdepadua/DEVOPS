package com.devops.devops.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author cesar
 * Reservation Entity
 */
@Entity
@Table(name = "reservations")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
allowGetters = true)
public class Reservation implements Serializable {

	/**
	 * Serial UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/** Reservation id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/** User. */
	
	@NotNull
	private Integer userId;
	
	@NotBlank
	private String firstNameUser;
	
	@NotBlank
	private String lastNameUser;
	
	/** Room. */
	
	/** Otions. */
	
	/** From Date. */
	@NotNull
	private Date fromDate;
	
	/** To Date. */
	@NotNull
	private Date toDate;
	
	/** Total Price. */
	@NotNull
	private Integer totalPrice;
	
	
	/** createdAt creation date. */
	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt;
	
	/** updateAt update date. */
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updateAt;
	
	@NotNull
	@OneToMany(cascade=CascadeType.ALL, mappedBy="reservation") 
	private List<RoomReservation> roomReservationList;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="reservation") 
	private List<OptionReservation> optionReservationList;
	
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		this.id = id;
	}
	
	
	
	/**
	 * @return the firstNameUser
	 */
	public String getFirstNameUser() {
		return firstNameUser;
	}

	/**
	 * @param firstNameUser the firstNameUser to set
	 */
	public void setFirstNameUser(String firstNameUser) {
		this.firstNameUser = firstNameUser;
	}

	/**
	 * @return the lastNameUser
	 */
	public String getLastNameUser() {
		return lastNameUser;
	}

	/**
	 * @param lastNameUser the lastNameUser to set
	 */
	public void setLastNameUser(String lastNameUser) {
		this.lastNameUser = lastNameUser;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(final Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(final Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the totalPrice
	 */
	public Integer getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(final Integer totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(final Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updateAt
	 */
	public Date getUpdateAt() {
		return updateAt;
	}

	/**
	 * @param updateAt the updateAt to set
	 */
	public void setUpdateAt(final Date updateAt) {
		this.updateAt = updateAt;
	}

	/**
	 * @return the roomReservationList
	 */
	public List<RoomReservation> getRoomReservationList() {
		return roomReservationList;
	}

	/**
	 * @param roomReservationList the roomReservationList to set
	 */
	public void setRoomReservationList(List<RoomReservation> roomReservationList) {
		this.roomReservationList = roomReservationList;
	}

	/**
	 * @return the optionReservationList
	 */
	public List<OptionReservation> getOptionReservationList() {
		return optionReservationList;
	}

	/**
	 * @param optionReservationList the optionReservationList to set
	 */
	public void setOptionReservationList(List<OptionReservation> optionReservationList) {
		this.optionReservationList = optionReservationList;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}
