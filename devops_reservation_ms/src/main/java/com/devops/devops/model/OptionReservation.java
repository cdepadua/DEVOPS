package com.devops.devops.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "option_reservations")
@EntityListeners(AuditingEntityListener.class)
public class OptionReservation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	private Long idOption;
	
	@NotNull
	private String cdHotel;
	
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Reservation reservation;

	/**
	 * @return the reservation
	 */
	public Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation the reservation to set
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * @return the idOption
	 */
	public Long getIdOption() {
		return idOption;
	}

	/**
	 * @param idOption the idOption to set
	 */
	public void setIdOption(Long idOption) {
		this.idOption = idOption;
	}

	/**
	 * @return the cdHotel
	 */
	public String getCdHotel() {
		return cdHotel;
	}

	/**
	 * @param cdHotel the cdHotel to set
	 */
	public void setCdHotel(String cdHotel) {
		this.cdHotel = cdHotel;
	}


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
