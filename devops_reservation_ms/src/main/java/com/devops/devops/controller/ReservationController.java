package com.devops.devops.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.devops.devops.converter.ReservationDtoConverter;
import com.devops.devops.dto.ReservationDto;
import com.devops.devops.service.IReservationService;

@RestController
@RequestMapping("/reservation-ms/reservation")
public class ReservationController {
	@Autowired
	private IReservationService reservationService;
	
	@RequestMapping(value = "/userauth/reserve", method = RequestMethod.POST)
	public final ReservationDto reserve(@RequestBody ReservationDto res){
		return ReservationDtoConverter.toDTO(this.reservationService.createReservation(res));
	}
}
