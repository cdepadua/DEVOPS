package com.devops.devops.dto;

import java.util.Date;
import java.util.List;

public class ReservationDto {
	
	private Integer id;
	
	private Integer userId;
	
	private String firstNameUser;
	
	private String lastNameUser;
	
	private Date fromDate;
	
	private Date toDate;
	
	private String cdHotel;
	
	private Integer totalPrice;
	
	private List<RoomsDto> listRoom;
	
	private List<OptionsDto> listOption;
	
	private List<RoomForReservationDto> listRoomAfterSave;
	
	private List<OptionForReservationDto> listOptionAfterSave;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return the firstNameUser
	 */
	public String getFirstNameUser() {
		return firstNameUser;
	}

	/**
	 * @param firstNameUser the firstNameUser to set
	 */
	public void setFirstNameUser(String firstNameUser) {
		this.firstNameUser = firstNameUser;
	}

	/**
	 * @return the lastNameUser
	 */
	public String getLastNameUser() {
		return lastNameUser;
	}

	/**
	 * @param lastNameUser the lastNameUser to set
	 */
	public void setLastNameUser(String lastNameUser) {
		this.lastNameUser = lastNameUser;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the totalPrice
	 */
	public Integer getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the listRoom
	 */
	public List<RoomsDto> getListRoom() {
		return listRoom;
	}

	/**
	 * @param listRoom the listRoom to set
	 */
	public void setListRoom(List<RoomsDto> listRoom) {
		this.listRoom = listRoom;
	}

	/**
	 * @return the listOption
	 */
	public List<OptionsDto> getListOption() {
		return listOption;
	}

	/**
	 * @param listOption the listOption to set
	 */
	public void setListOption(List<OptionsDto> listOption) {
		this.listOption = listOption;
	}

	/**
	 * @return the cdHotel
	 */
	public String getCdHotel() {
		return cdHotel;
	}

	/**
	 * @param cdHotel the cdHotel to set
	 */
	public void setCdHotel(String cdHotel) {
		this.cdHotel = cdHotel;
	}

	/**
	 * @return the listRoomAfterSave
	 */
	public List<RoomForReservationDto> getListRoomAfterSave() {
		return listRoomAfterSave;
	}

	/**
	 * @param listRoomAfterSave the listRoomAfterSave to set
	 */
	public void setListRoomAfterSave(List<RoomForReservationDto> listRoomAfterSave) {
		this.listRoomAfterSave = listRoomAfterSave;
	}

	/**
	 * @return the listOptionAfterSave
	 */
	public List<OptionForReservationDto> getListOptionAfterSave() {
		return listOptionAfterSave;
	}

	/**
	 * @param listOptionAfterSave the listOptionAfterSave to set
	 */
	public void setListOptionAfterSave(List<OptionForReservationDto> listOptionAfterSave) {
		this.listOptionAfterSave = listOptionAfterSave;
	}
	
	
}
