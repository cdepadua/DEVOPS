package com.devops.devops.dto;

/**
 * @author cesar
 * OptionsDto
 */
public class OptionsDto {
	
	/** Option Id. */
	private long id;
	
	/** Option name. */
	private String optionName;
	
	/** Available number. */
	private Integer number;
	

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * @return the optionName
	 */
	public String getOptionName() {
		return optionName;
	}

	/**
	 * @param optionName the optionName to set
	 */
	public void setOptionName(final String optionName) {
		this.optionName = optionName;
	}

	/**
	 * @return the number
	 */
	public Integer getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(final Integer number) {
		this.number = number;
	}	
	
}
