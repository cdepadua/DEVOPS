package com.devops.devops.dto;

public class RoomForReservationDto {
	private Long id;
	private String type;
	private Integer price;
	private String cdHotel;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the price
	 */
	public Integer getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}
	/**
	 * @return the cdHotel
	 */
	public String getCdHotel() {
		return cdHotel;
	}
	/**
	 * @param cdHotel the cdHotel to set
	 */
	public void setCdHotel(String cdHotel) {
		this.cdHotel = cdHotel;
	}
	
	
}
