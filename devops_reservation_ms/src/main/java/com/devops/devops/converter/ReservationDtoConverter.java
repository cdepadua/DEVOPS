package com.devops.devops.converter;

import java.util.ArrayList;
import java.util.List;

import com.devops.devops.dto.OptionForReservationDto;
import com.devops.devops.dto.OptionsDto;
import com.devops.devops.dto.ReservationDto;
import com.devops.devops.dto.RoomForReservationDto;
import com.devops.devops.dto.RoomsDto;
import com.devops.devops.model.OptionReservation;
import com.devops.devops.model.Reservation;
import com.devops.devops.model.RoomReservation;

public final class ReservationDtoConverter {
	
	private ReservationDtoConverter(){}
	
	public static Reservation toBIZ(ReservationDto reservationDto) {
		Reservation reservation = new Reservation();
		reservation.setFirstNameUser(reservationDto.getFirstNameUser());
		reservation.setLastNameUser(reservationDto.getLastNameUser());
		reservation.setUserId(reservationDto.getUserId());
		reservation.setFromDate(reservationDto.getFromDate());
		reservation.setToDate(reservationDto.getToDate());
		return reservation;
	}
	
	public static ReservationDto toDTO(Reservation reservation) {
		ReservationDto reservationDto = new ReservationDto();
		reservationDto.setFirstNameUser(reservation.getFirstNameUser());
		reservationDto.setLastNameUser(reservation.getLastNameUser());
		reservationDto.setUserId(reservation.getUserId());
		reservationDto.setFromDate(reservation.getFromDate());
		reservationDto.setToDate(reservation.getToDate());
		reservationDto.setTotalPrice(reservation.getTotalPrice());
		List<RoomForReservationDto> rooms = new ArrayList<>();
		reservation.getRoomReservationList().forEach(room ->{
			RoomForReservationDto roomDto = new RoomForReservationDto();
			roomDto.setCdHotel(room.getCdHotel());
			roomDto.setId(room.getIdRoom());
			rooms.add(roomDto);
		});
		reservationDto.setListRoomAfterSave(rooms);
		List<OptionForReservationDto> options = new ArrayList<>();
		reservation.getOptionReservationList().forEach(option ->{
			OptionForReservationDto optionDto = new OptionForReservationDto();
			optionDto.setCdHotel(option.getCdHotel());
			optionDto.setId(option.getIdOption());
			options.add(optionDto);
		});
		reservationDto.setListOptionAfterSave(options);
		return reservationDto;
	}
}
