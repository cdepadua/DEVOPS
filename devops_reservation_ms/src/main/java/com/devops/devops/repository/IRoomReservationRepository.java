package com.devops.devops.repository;

import java.util.Date;
import java.util.List;

import com.devops.devops.model.RoomReservation;

public interface IRoomReservationRepository {

	RoomReservation createRoomReservation(RoomReservation roomReservation);

	List<RoomReservation> getNotEmptyRoomByDate(Date from, Date to, String cdHotel);
}
