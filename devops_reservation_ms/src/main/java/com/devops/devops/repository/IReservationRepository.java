package com.devops.devops.repository;

import org.springframework.stereotype.Repository;

import com.devops.devops.model.Reservation;

/**
 * @author cesar
 * Interface reservation repository
 */

public interface IReservationRepository {
	
	/**
	 * create a reservation
	 * @param reservation
	 * @return created reservation
	 */
	Reservation createReservation(Reservation reservation);
}
