package com.devops.devops.repository;

import java.util.Date;
import java.util.List;

import com.devops.devops.model.OptionReservation;

public interface IOptionReservationRepository {
	
	OptionReservation createOptionReservation(OptionReservation optionReservation);

	List<OptionReservation> getNotEmptyOptionByDate(Date from, Date to, String cdHotel);
}
