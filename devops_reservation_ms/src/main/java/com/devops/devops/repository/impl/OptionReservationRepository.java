package com.devops.devops.repository.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.devops.devops.model.OptionReservation;
import com.devops.devops.model.RoomReservation;
import com.devops.devops.repository.AbstractRepository;
import com.devops.devops.repository.IOptionReservationRepository;

@Repository
public class OptionReservationRepository extends AbstractRepository implements IOptionReservationRepository{

	@Override
	public OptionReservation createOptionReservation(OptionReservation optionReservation) {
		this.getEm().persist(optionReservation);
		this.getEm().flush();
		return optionReservation;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<OptionReservation> getNotEmptyOptionByDate(Date from, Date to, String cdHotel) {
		return (List<OptionReservation>) this.getEm()
				.createNativeQuery("SELECT * FROM (SELECT cd_hotel, id_option, r.from_date, r.to_date FROM option_reservations rr INNER JOIN reservations r ON rr.reservation_id = r.id) res WHERE cd_hotel = :cdHotel and :fromDateP BETWEEN from_date AND to_date OR :toDateP BETWEEN from_date AND to_date GROUP BY id_option", OptionReservation.class)
				.setParameter("cdHotel", cdHotel).setParameter("fromDateP", from).setParameter("toDateP", to).getResultList();
	}
}
