package com.devops.devops.repository.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.devops.devops.model.RoomReservation;
import com.devops.devops.repository.AbstractRepository;
import com.devops.devops.repository.IRoomReservationRepository;

@Repository
public class RoomReservationRepository extends AbstractRepository implements IRoomReservationRepository{

	@Override
	public RoomReservation createRoomReservation(RoomReservation roomReservation) {
		this.getEm().persist(roomReservation);
		this.getEm().flush();
		return roomReservation;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RoomReservation> getNotEmptyRoomByDate(Date from, Date to, String cdHotel) {
		return (List<RoomReservation>) this.getEm()
				.createNativeQuery("SELECT * FROM (SELECT cd_hotel, id_room, r.from_date, r.to_date FROM room_reservations rr INNER JOIN reservations r ON rr.reservation_id = r.id) res WHERE cd_hotel = :cdHotel and :fromDateP BETWEEN from_date AND to_date OR :toDateP BETWEEN from_date AND to_date GROUP BY id_room", RoomReservation.class)
				.setParameter("cdHotel", cdHotel).setParameter("fromDateP", from).setParameter("toDateP", to).getResultList();
	}


}
