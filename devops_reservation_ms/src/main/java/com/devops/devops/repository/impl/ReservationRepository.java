package com.devops.devops.repository.impl;

import org.springframework.stereotype.Repository;

import com.devops.devops.model.Reservation;
import com.devops.devops.repository.AbstractRepository;
import com.devops.devops.repository.IReservationRepository;

/**
 * @author cesar
 * Reservation repository
 */
@Repository
public class ReservationRepository extends AbstractRepository implements IReservationRepository {

	/** {@IneritDoc.} */
	@Override
	public Reservation createReservation(Reservation reservation) {
		this.getEm().persist(reservation);
		this.getEm().flush();
		return reservation;
	}

}
