package com.devops.devops.controllerit;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.devops.devops.dto.HotelDto;
import com.devops.devops.dto.OptionsDto;
import com.devops.devops.dto.RoomsDto;
import com.devops.devops.model.Hotel;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class hotelControlleIt {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	 @Value("${back.api.uri}")
	 String uri;
	
	@Test
	public void getHotelsTest() {
		ResponseEntity<Hotel[]> res = this.restTemplate.getForEntity(uri + "/hotel-ms/hotel/find_all_hotels", Hotel[].class);
		List<Hotel> hotels = Arrays.asList(res.getBody());
		
		assertEquals(hotels.size(), 2);
	}
	
	@Test
	public void getHotelByCdHotelTest() {
		ResponseEntity<Hotel> res =  this.restTemplate.getForEntity(uri + "/hotel-ms/hotel/get_hotel_by_code/H1", Hotel.class);
		Hotel hotel = res.getBody();
		
		assertEquals(hotel.getCdHotel(), "H1");
	}

}
