package com.devops.devops.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author cesar
 * Room entity
 */
@Entity
@Table(name = "rooms")
@EntityListeners(AuditingEntityListener.class)
public class Room implements Serializable {

	/**
	 * Serial UID. 
	 */
	private static final long serialVersionUID = 1L;

	/** Room Id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/** Room type. */
	@NotNull
	@ManyToOne(cascade = CascadeType.ALL)
	private RoomType roomType;
	
	/** Hotel. */
	@NotNull
	@ManyToOne(cascade = CascadeType.ALL)
	private Hotel hotel;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * @return the roomType
	 */
	public RoomType getRoomType() {
		return roomType;
	}

	/**
	 * @param roomType the roomType to set
	 */
	public void setRoomType(final RoomType roomType) {
		this.roomType = roomType;
	}

	/**
	 * @return the hotel
	 */
	public Hotel getHotel() {
		return hotel;
	}

	/**
	 * @param hotel the hotel to set
	 */
	public void setHotel(final Hotel hotel) {
		this.hotel = hotel;
	}
}
