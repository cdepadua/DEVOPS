package com.devops.devops.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author cesar
 * OptionType entity
 */
@Entity
@Table (name = "option_types")
@EntityListeners(AuditingEntityListener.class)
public class OptionType implements Serializable {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;
	
	/** Option id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/** Label. */
	@NotBlank
	@Column(unique = true)
	private String label;
	
	/** Option Code. */
	@NotBlank
	@Column(unique = true)
	private String cdOption;
	
	/** Price. */
	@NotNull
	private Integer price;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(final String label) {
		this.label = label;
	}
	
	/**
	 * @return the cdOption
	 */
	public String getCdOption() {
		return cdOption;
	}

	/**
	 * @param cdOption the cdOption to set
	 */
	public void setCdOption(final String cdOption) {
		this.cdOption = cdOption;
	}

	/**
	 * @return the price
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(final Integer price) {
		this.price = price;
	}
	
}
