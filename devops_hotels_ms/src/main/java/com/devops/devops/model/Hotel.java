package com.devops.devops.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author cesar
 * Hotel entity
 */
@Entity
@Table(name = "hotels")
@EntityListeners(AuditingEntityListener.class)
public class Hotel implements Serializable {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;
	
	/** Hotel id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/** Hotel name. */
	@NotBlank
	private String name;
	
	/** Hotel code. */
	@NotBlank
	@Column(unique = true)
	private String cdHotel;
	
	/** Hotel address. */
	@NotBlank
	@Column(unique = true)
	private String address;
	
	/** Hotel's e-mail. */
	@NotBlank
	private String email;
	
	/** Hotel phone number. */
	@NotBlank
	private String phoneNumber;
	
	/** Rooms. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="hotel") 
	private List<Room> rooms;
	
	/** Options. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="hotel") 
	private List<Option> options;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return the cdHotel
	 */
	public String getCdHotel() {
		return cdHotel;
	}

	/**
	 * @param cdHotel the cdHotel to set
	 */
	public void setCdHotel(final String cdHotel) {
		this.cdHotel = cdHotel;
	}
	
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(final String address) {
		this.address = address;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @return the rooms
	 */
	public List<Room> getRooms() {
		return rooms;
	}

	/**
	 * @param rooms the rooms to set
	 */
	public void setRooms(final List<Room> rooms) {
		this.rooms = rooms;
	}

	/**
	 * @return the options
	 */
	public List<Option> getOptions() {
		return options;
	}

	/**
	 * @param options the options to set
	 */
	public void setOptions(final List<Option> options) {
		this.options = options;
	}
}
