package com.devops.devops.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author cesar
 * Option entity
 */
@Entity
@Table (name = "options")
@EntityListeners(AuditingEntityListener.class)
public class Option implements Serializable{

	/**
	 * Serial UID 
	 */
	private static final long serialVersionUID = 1L;

	/** Option id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/** Label. */
	@NotNull
	@ManyToOne(cascade = CascadeType.ALL)
	private OptionType optionType;
	
	/** Hotel. */
	@NotNull
	@ManyToOne(cascade = CascadeType.ALL)
	private Hotel hotel;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * @return the optionType
	 */
	public OptionType getOptionType() {
		return optionType;
	}

	/**
	 * @param optionType the optionType to set
	 */
	public void setOptionType(final OptionType optionType) {
		this.optionType = optionType;
	}
	

	/**
	 * @return the hotel
	 */
	public Hotel getHotel() {
		return hotel;
	}

	/**
	 * @param hotel the hotel to set
	 */
	public void setHotel(final Hotel hotel) {
		this.hotel = hotel;
	}
}
