package com.devops.devops.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author cesar
 *	RoomType entity
 */
@Entity
@Table(name = "rooms_type")
@EntityListeners(AuditingEntityListener.class)
public class RoomType implements Serializable{

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;
	
	/** Id room. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/** Label. */
	@NotBlank
	String label;
	
	/** Room code. */
	@NotBlank
	String cdRoom;
	
	/** Maximal number of people. */
	@NotNull
	Integer nbOfPeople;
	
	/** Room price. */
	@NotNull
	Integer price;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * @return the cdRoom
	 */
	public String getCdRoom() {
		return cdRoom;
	}

	/**
	 * @param cdRoom the cdRoom to set
	 */
	public void setCdRoom(final String cdRoom) {
		this.cdRoom = cdRoom;
	}

	/**
	 * @return the nbOfPeople
	 */
	public Integer getNbOfPeople() {
		return nbOfPeople;
	}

	/**
	 * @param nbOfPeople the nbOfPeople to set
	 */
	public void setNbOfPeople(final Integer nbOfPeople) {
		this.nbOfPeople = nbOfPeople;
	}

	/**
	 * @return the price
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(final Integer price) {
		this.price = price;
	}
	
}
