package com.devops.devops.dto;

import java.util.List;

/**
 * @author cesar
 * HotelDto
 */
public class HotelDto {
	
	/** Hotel id. */
	private long id;
	
	/** Hotel name. */
	private String name;
	
	/** Hotel code. */
	private String cdHotel;
	
	/** Hotel address. */
	private String address;
	
	/** Hotel phone number. */
	private String phoneNumber;
	
	/** Hotel's e-mail. */
	private String email;
	
	/** List of available rooms. */
	private List<RoomsDto> rooms;
	
	/** List of available options. */
	private List<OptionsDto> options;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return the cdHotel
	 */
	public String getCdHotel() {
		return cdHotel;
	}

	/**
	 * @param cdHotel the cdHotel to set
	 */
	public void setCdHotel(final String cdHotel) {
		this.cdHotel = cdHotel;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(final String address) {
		this.address = address;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @return the rooms
	 */
	public List<RoomsDto> getRooms() {
		return rooms;
	}

	/**
	 * @param rooms the rooms to set
	 */
	public void setRooms(final List<RoomsDto> rooms) {
		this.rooms = rooms;
	}

	/**
	 * @return the options
	 */
	public List<OptionsDto> getOptions() {
		return options;
	}

	/**
	 * @param options the options to set
	 */
	public void setOptions(final List<OptionsDto> options) {
		this.options = options;
	}
	
}
