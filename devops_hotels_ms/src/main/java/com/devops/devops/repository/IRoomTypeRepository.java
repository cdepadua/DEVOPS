package com.devops.devops.repository;

import java.util.HashMap;

import com.devops.devops.model.RoomType;

/**
 * @author cesar
 * Interface Room type repository
 */
public interface IRoomTypeRepository {
	/**
	 * add a new room type
	 * @param roomType 
	 * @return created room type
	 */
	RoomType addNewRoomType(RoomType roomType);
	
	/**
	 * return all roomtype as an hashmap
	 * @return all room type
	 */
	HashMap<String, RoomType> getAllRoomTypeHasHashMap();
}
