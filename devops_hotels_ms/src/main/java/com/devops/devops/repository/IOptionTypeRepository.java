package com.devops.devops.repository;

import java.util.HashMap;

import com.devops.devops.model.OptionType;

/**
 * @author cesar
 * Interface Option type repository
 */
public interface IOptionTypeRepository {
	/**
	 * Create a new option type
	 * @param optionType
	 * @return created option type
	 */
	OptionType createNewOption(OptionType optionType);
	
	/**
	 * Return all option type as an hashmap
	 * @return option type
	 */
	HashMap<String, OptionType> getAllOptionTypeAsHashMap();
}
