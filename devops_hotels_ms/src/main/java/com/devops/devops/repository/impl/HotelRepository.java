package com.devops.devops.repository.impl;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.devops.devops.model.Hotel;
import com.devops.devops.model.Option;
import com.devops.devops.model.Room;
import com.devops.devops.repository.AbstractRepository;
import com.devops.devops.repository.IHotelRepository;

/**
 * @author cesar
 * Hotel repository
 */
@Repository
public class HotelRepository extends AbstractRepository implements IHotelRepository{
	
	/** {@IneritDoc}. */
	@SuppressWarnings("unchecked")
	@Override
	public List<Hotel> findAllHotels()
	{
		return (List<Hotel>)this.getEm().createNativeQuery("SELECT * FROM hotels", Hotel.class).getResultList();
	}
	
	/** {@IneritDoc}. */
	@Override
	public Hotel getHotelsDetailsFromHotelCode (final String cd) throws NoResultException {
		Hotel hotel = null;
		try {
			hotel = (Hotel) this.getEm().createNativeQuery("SELECT * FROM hotels h where h.cd_hotel = :cd", Hotel.class)
					.setParameter("cd", cd).getSingleResult();
		}catch(NoResultException noresult){
			throw new NoResultException();
		}
		return hotel;
	}

	/** {@IneritDoc}. */
	@Override
	public Hotel createHotel(Hotel hotel) {
		this.getEm().persist(hotel);
		this.getEm().flush();
		return hotel;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Room> getRoomsByCdHotel(Long cdHotel) {
		return this.getEm().createNativeQuery("SELECT * FROM rooms WHERE hotel_id = :cdHotel", Room.class)
				.setParameter("cdHotel", cdHotel).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Option> getOptionsByCdHotel(Long hotelId) {
		return this.getEm().createNativeQuery("SELECT * FROM options WHERE hotel_id = :hotelId", Option.class)
				.setParameter("hotelId", hotelId).getResultList();
	}
}
