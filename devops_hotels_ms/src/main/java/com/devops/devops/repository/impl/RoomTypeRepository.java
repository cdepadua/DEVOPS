package com.devops.devops.repository.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.devops.devops.model.RoomType;
import com.devops.devops.repository.AbstractRepository;
import com.devops.devops.repository.IRoomTypeRepository;

/**
 * @author cesar
 * Room type repository
 */
@Repository
public class RoomTypeRepository extends AbstractRepository implements IRoomTypeRepository {
	
	/** {@IneritDoc}. */
	@Override
	public RoomType addNewRoomType(RoomType myRoomType) {
		this.getEm().persist(myRoomType);
		this.getEm().flush();
		return myRoomType;
	}

	@Override
	public HashMap<String, RoomType> getAllRoomTypeHasHashMap() {
		@SuppressWarnings("unchecked")
		List<RoomType> roomTypes = (List<RoomType>)this.getEm()
				.createNativeQuery("SELECT * FROM rooms_type", RoomType.class).getResultList();
		HashMap<String, RoomType> valueToReturn = new HashMap<>();
		roomTypes.forEach(roomType -> {
			valueToReturn.put(roomType.getCdRoom(), roomType);
		});
		return valueToReturn;
	}
}
