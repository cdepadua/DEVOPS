package com.devops.devops.repository.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.devops.devops.model.OptionType;
import com.devops.devops.repository.AbstractRepository;
import com.devops.devops.repository.IOptionTypeRepository;

/**
 * @author cesar
 * Optiontype repository
 */
@Repository
public class OptionTypeRepository extends AbstractRepository implements IOptionTypeRepository {
	/** {@IneritDoc}. */
	@Override
	public OptionType createNewOption(OptionType optionType){
		this.getEm().persist(optionType);
		this.getEm().flush();
		return optionType;
	}

	/** {@IneritDoc}. */
	@Override
	public HashMap<String, OptionType> getAllOptionTypeAsHashMap() {
		@SuppressWarnings("unchecked")
		List<OptionType> optionTypes = (List<OptionType>)this.getEm()
				.createNativeQuery("SELECT * FROM option_types", OptionType.class).getResultList();
		HashMap<String, OptionType> valueToReturn = new HashMap<>();
		optionTypes.forEach(optionType -> {
			valueToReturn.put(optionType.getCdOption(), optionType);
		});
		return valueToReturn;
	}
}
