package com.devops.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author cesar
 *
 */
@SpringBootApplication
@EnableJpaAuditing
@PropertySources({@PropertySource("classpath:application.properties"),
		@PropertySource( value = "", ignoreResourceNotFound = true)})
public class DevopsApplicationHotelsMs {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(DevopsApplicationHotelsMs.class, args);
	}
}
