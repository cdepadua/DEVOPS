package com.devops.devops.service.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devops.devops.model.OptionType;
import com.devops.devops.repository.IOptionTypeRepository;
import com.devops.devops.service.IOptionService;

/**
 * @author cesar
 * Option service
 */
@Service
@Transactional(rollbackFor = {RuntimeException.class})
public class OptionService implements IOptionService{
	
	/** Option type repository. */
	@Autowired
	private IOptionTypeRepository optionTypeRepository;
	
	/** {@IneritDoc}. */
	@Override
	public OptionType createNewOptionType(final OptionType optionType)
	{
		return this.optionTypeRepository.createNewOption(optionType);
	}
	
	/** {@IneritDoc}. */
	@Override
	public HashMap<String, OptionType> getAllOptionTypeAsHashMap() {
		return this.optionTypeRepository.getAllOptionTypeAsHashMap();
	}
}
