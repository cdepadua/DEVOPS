package com.devops.devops.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.devops.devops.model.Hotel;
import com.devops.devops.model.Option;
import com.devops.devops.model.Room;
import com.devops.devops.repository.IHotelRepository;
import com.devops.devops.service.IHotelService;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author cesar
 * Hotel service
 */
@Service
@Transactional(rollbackFor = {RuntimeException.class})
public class HotelService implements IHotelService{
	
	/** Hotel repository. */
	@Autowired
	private IHotelRepository hotelRepository;

	/** {@IneritDoc}. */
	@Override
	public Hotel createHotel(Hotel hotel) {
		return this.hotelRepository.createHotel(hotel);
	}
	
	/** {@IneritDoc}. */
	@Override
	public List<Hotel> findAllHotels() {
		return this.hotelRepository.findAllHotels();
	}

	/** {@IneritDoc}. */
	@Override
	public Hotel getHotelsDetailsFromHotelCode(String cd) throws NoResultException {
		return this.hotelRepository.getHotelsDetailsFromHotelCode(cd);
	}
	
	@Override
	public List<Room> getRoomsByCdHotel(Long cdHotel) {
		return this.hotelRepository.getRoomsByCdHotel(cdHotel);
	}
	
	@Override
	public List<Option> getOptionsByCdHotel(Long hotelId) {
		return this.hotelRepository.getOptionsByCdHotel(hotelId);
	}
}
