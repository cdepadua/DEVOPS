package com.devops.devops.service;

import java.util.HashMap;

import com.devops.devops.model.RoomType;

/**
 * @author cesar
 * Interface room service
 */
public interface IRoomService {
	
	/**
	 * Add a new room type
	 * @param myRoomType
	 * @return created room type
	 */
	RoomType addNewRoomType(RoomType myRoomType);
	
	/**
	 * return all roomtype as an hashmap
	 * @return all room type
	 */
	HashMap<String, RoomType> getAllRoomTypeHasHashMap();
}
