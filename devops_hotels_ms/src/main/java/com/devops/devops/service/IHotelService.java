package com.devops.devops.service;

import java.util.List;

import javax.persistence.NoResultException;

import com.devops.devops.model.Hotel;
import com.devops.devops.model.Option;
import com.devops.devops.model.Room;

/**
 * @author cesar
 * Interface hotel service
 */
public interface IHotelService {
	
	/**
	 * Create a new hotel
	 * @param hotel
	 * @return created hotel
	 */
	Hotel createHotel(Hotel hotel); 
	
	/**
	 * Retourns all hotels in bdd
	 * @return hotels
	 */
	List<Hotel> findAllHotels();
	/**
	 * Return the hotel's details from given hotel code
	 * @param cd hotel code
	 * @return hotel details
	 * @throws NoResultException 
	 */
	Hotel getHotelsDetailsFromHotelCode (String cd) throws NoResultException;

	List<Room> getRoomsByCdHotel(Long idHotel);

	List<Option> getOptionsByCdHotel(Long idHotel);
}
