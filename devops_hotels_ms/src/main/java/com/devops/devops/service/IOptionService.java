package com.devops.devops.service;

import java.util.HashMap;

import com.devops.devops.model.OptionType;

/**
 * @author cesar
 * Interface Option service
 */
public interface IOptionService {
	/**
	 * create a new option type
	 * @param optionType
	 * @return created option type
	 */
	OptionType createNewOptionType(OptionType optionType);
	
	/**
	 * Return all option type as an hashmap
	 * @return option type
	 */
	HashMap<String, OptionType> getAllOptionTypeAsHashMap();
}
