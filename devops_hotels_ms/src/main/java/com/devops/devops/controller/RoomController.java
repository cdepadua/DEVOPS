package com.devops.devops.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.devops.devops.converter.RoomTypeDtoConverter;
import com.devops.devops.dto.RoomTypeDto;
import com.devops.devops.service.IRoomService;

/**
 * @author cesar
 * Room Controller
 */
@RestController
@RequestMapping("/hotel-ms/room")
public class RoomController {

	/** Room service. */
	@Autowired
	IRoomService roomService;
	
	/**
	 * Add a new room type
	 * @param roomTypeDto
	 * @return created room type
	 */
	@RequestMapping(value = "/adminauth/add_room_type", method = RequestMethod.POST)
	public final RoomTypeDto addRoomType(@RequestBody RoomTypeDto roomTypeDto) {
		return RoomTypeDtoConverter.toDTO(this.roomService
				.addNewRoomType(RoomTypeDtoConverter.toBIZ(roomTypeDto)));
	}
}
