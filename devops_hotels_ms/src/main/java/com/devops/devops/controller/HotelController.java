package com.devops.devops.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.devops.devops.converter.HotelDtoConverter;
import com.devops.devops.converter.OptionForReservationDtoConverter;
import com.devops.devops.converter.RoomsForReservationDtoConverter;
import com.devops.devops.dto.HotelDto;
import com.devops.devops.dto.OptionForReservationDto;
import com.devops.devops.dto.RoomForReservationDto;
import com.devops.devops.model.Hotel;
import com.devops.devops.service.IHotelService;
import com.devops.devops.service.IOptionService;
import com.devops.devops.service.IRoomService;

/**
 * @author cesar
 * HotelController
 */
@RestController
@RequestMapping("/hotel-ms/hotel")
public class HotelController {
	/** Hotel Service. */
	@Autowired
	private IHotelService hotelService;
	
	/** Option service. */
	@Autowired
	private IOptionService optionService;
	
	/** Room service. */
	@Autowired
	private IRoomService roomService;
	
	/**
	 * create a new hotel
	 * @param hotelDto hotel dto
	 * @return created hotel
	 */
	@RequestMapping(value = "/adminauth/add_hotel", method = RequestMethod.POST)
	public final HotelDto addAnHotel(@RequestBody final HotelDto hotelDto) {
		Hotel hotel = this.hotelService.createHotel(HotelDtoConverter.toBIZ(hotelDto, 
				this.roomService.getAllRoomTypeHasHashMap(), 
				this.optionService.getAllOptionTypeAsHashMap()));
		return HotelDtoConverter.toDTO(hotel);
	}
	
	/**
	 * Return all hotels
	 * @return hotels
	 */
	@RequestMapping(value = "/find_all_hotels", method = RequestMethod.GET)
	public final List<HotelDto> findAllHotels() {
		List<Hotel> hotels = this.hotelService.findAllHotels();
		ArrayList<HotelDto> hotelList = new ArrayList<>();
		hotels.forEach(hotel -> {
			hotelList.add(HotelDtoConverter.toDTO(hotel));
		});
		return hotelList;
	}
	
	/**
	 * return hotel details from hotel code
	 * @param cdHotel
	 * @return hotel details
	 */
	@RequestMapping(value = "/get_hotel_by_code/{cdHotel}", method = RequestMethod.GET)
	public HotelDto findHotelByCode(@PathVariable final String cdHotel) {
		return HotelDtoConverter.toDTO(this.hotelService.getHotelsDetailsFromHotelCode(cdHotel));
	}
	
	@RequestMapping(value = "/get_rooms_by_cd_hotel/{cdHotel}", method = RequestMethod.GET)
	public List<RoomForReservationDto> getRoomsByCdHotel(@PathVariable final String cdHotel) {
		List<RoomForReservationDto> rooms = new ArrayList<>();
		Long idHotel = this.hotelService.getHotelsDetailsFromHotelCode(cdHotel).getId();
		this.hotelService.getRoomsByCdHotel(idHotel).forEach(room ->{
			rooms.add(RoomsForReservationDtoConverter.toDTO(room));
		});
		return rooms;
	}
	
	@RequestMapping(value = "/get_options_by_cd_hotel/{cdHotel}", method = RequestMethod.GET)
	public final List<OptionForReservationDto> getOptionsByCdHotel(@PathVariable final String cdHotel) {
		List<OptionForReservationDto> options = new ArrayList<>();
		Long idHotel = this.hotelService.getHotelsDetailsFromHotelCode(cdHotel).getId();
		this.hotelService.getOptionsByCdHotel(idHotel).forEach(option ->{
			options.add(OptionForReservationDtoConverter.toDTO(option));
		});
		return options;
	}
}
