package com.devops.devops.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cdepadua on 09/02/2018.
 */
public class JwtHeaderPreAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {

    private static final Logger LOG = LoggerFactory.getLogger(JwtHeaderPreAuthenticationFilter.class);

    private static final String PREFIX_ROLE = "ROLE_";

    @Value("${gateway.security.jwt.header}")
    private String jwtHeader;

    @Value("${gateway.security.jwt.secret}")
    private String jwtSecret;

    @Override
    protected AuthenticationDetailsSource<HttpServletRequest, ?> getAuthenticationDetailsSource() {
        // TODO Auto-generated method stub
        return super.getAuthenticationDetailsSource();
    }

    @Override
    protected Object getPreAuthenticatedPrincipal(final HttpServletRequest request) {
        // Récupération du token
        final String compactJws = request.getHeader(this.jwtHeader);
        if (compactJws == null) {
            return null;
        }

        // Validation et extraction des données du token
        final Claims jwt = Jwts.parser().setSigningKey(this.jwtSecret).parseClaimsJws(compactJws).getBody();

        // Récupération de l'UID de l'utilisateur et de son profile actif
        final String subject = jwt.getSubject();
        final String profile = jwt.get("profile", String.class);
        final List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(this.PREFIX_ROLE + profile));

        // Renvoi des informations de connexion
        return new User(subject, "", grantedAuthorities);
    }

    @Override
    protected Object getPreAuthenticatedCredentials(final HttpServletRequest request) {
        return "N/A";
    }
}
