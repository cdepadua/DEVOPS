package com.devops.devops.security;

import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
public class PreAuthenticationUserDetailsService implements 
	AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken>{

	@Override
	public UserDetails loadUserDetails(
			final PreAuthenticatedAuthenticationToken preAuthenticatedAuthenticationToken) 
					throws UsernameNotFoundException {
		return  (UserDetails) preAuthenticatedAuthenticationToken.getPrincipal();
	}
	
}
