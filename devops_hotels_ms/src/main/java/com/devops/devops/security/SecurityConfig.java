package com.devops.devops.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;

/**
 * Created by cdepadua on 09/02/2018.
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    private static String REALM_NAME ="RESTFUL_REALM";

    @Autowired
    PreAuthenticationUserDetailsService preAuthenticationUserDetailsService;

    /**
     * @param auth
     * @throws Exception
     */
   

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests().antMatchers("/**").authenticated().anyRequest()
        .permitAll().and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        
        http.addFilterAfter(this.preAuthenticatedProcessingFilter(), AbstractPreAuthenticatedProcessingFilter.class);
        
        http.csrf().disable().authorizeRequests().antMatchers("/").permitAll().and()
        	.authorizeRequests().antMatchers("/h2/**").permitAll();
        
        http.headers().frameOptions().sameOrigin();

    }
    
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
    	auth.authenticationProvider(this.preAuthenticationProvider());
    }
    
    @Bean
    protected AbstractPreAuthenticatedProcessingFilter preAuthenticatedProcessingFilter() throws Exception {
    	final JwtHeaderPreAuthenticationFilter filter = new JwtHeaderPreAuthenticationFilter();
    	filter.setCheckForPrincipalChanges(true);
    	filter.setAuthenticationManager(this.authenticationManager());
    	return filter;
    }
    
    @Bean
    public AuthenticationProvider preAuthenticationProvider() {
    	final PreAuthenticatedAuthenticationProvider authenticationProvider = 
    			new PreAuthenticatedAuthenticationProvider();
    	authenticationProvider.setPreAuthenticatedUserDetailsService(this.preAuthenticationUserDetailsService);
    	authenticationProvider.setThrowExceptionWhenTokenRejected(true);
    	return authenticationProvider;
    }
}
