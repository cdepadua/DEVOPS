package com.devops.devops.converter;

import com.devops.devops.dto.RoomTypeDto;
import com.devops.devops.model.RoomType;

/**
 * @author cesar
 * Room type dto converter
 */
public final class RoomTypeDtoConverter {
	/** Constructor. */
	private RoomTypeDtoConverter(){}
	
	/**
	 * Convert a room type entity to room type dto
	 * @param roomType
	 * @return converted room type dto
	 */
	public static RoomTypeDto toDTO(RoomType roomType) {
		RoomTypeDto roomTypeDto = new RoomTypeDto();
		roomTypeDto.setId(roomType.getId());
		roomTypeDto.setCdRoom(roomType.getCdRoom());
		roomTypeDto.setLabel(roomType.getLabel());
		roomTypeDto.setNbOfPeople(roomType.getNbOfPeople());
		roomTypeDto.setPrice(roomType.getPrice());
		return roomTypeDto;
	}
	
	/**
	 * Convert a room type dto to room type entity
	 * @param roomTypeDto
	 * @return converted room type entity
	 */
	public static RoomType toBIZ(RoomTypeDto roomTypeDto) {
		RoomType roomType = new RoomType();
		roomType.setId(roomTypeDto.getId());
		roomType.setCdRoom(roomTypeDto.getCdRoom());
		roomType.setLabel(roomTypeDto.getLabel());
		roomType.setNbOfPeople(roomTypeDto.getNbOfPeople());
		roomType.setPrice(roomTypeDto.getPrice());
		return roomType;
	}
}
