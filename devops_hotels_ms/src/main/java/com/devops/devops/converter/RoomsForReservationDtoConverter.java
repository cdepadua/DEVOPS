package com.devops.devops.converter;

import com.devops.devops.dto.RoomForReservationDto;
import com.devops.devops.model.Room;

final public class RoomsForReservationDtoConverter {
	private RoomsForReservationDtoConverter(){}
	
	public static RoomForReservationDto toDTO(Room room) {
		RoomForReservationDto roomDto = new RoomForReservationDto();
		roomDto.setCdHotel(room.getHotel().getCdHotel());
		roomDto.setId(room.getId());
		roomDto.setType(room.getRoomType().getCdRoom());
		roomDto.setPrice(room.getRoomType().getPrice());
		return roomDto;
	}
}
