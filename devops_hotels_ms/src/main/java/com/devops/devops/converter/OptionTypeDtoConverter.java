package com.devops.devops.converter;

import com.devops.devops.dto.OptionTypeDto;
import com.devops.devops.model.OptionType;

/**
 * @author cesar
 * Option type dto converter
 */
public final class OptionTypeDtoConverter {
	/** Constructor. */
	private OptionTypeDtoConverter(){}
	
	/**
	 * Convert a option type entity to option type dto
	 * @param optionType
	 * @return converted option type dto
	 */
	public static OptionTypeDto toDTO(OptionType optionType) {
		OptionTypeDto optionTypeDto = new OptionTypeDto();
		optionTypeDto.setId(optionType.getId());
		optionTypeDto.setLabel(optionType.getLabel());
		optionTypeDto.setPrice(optionType.getPrice());
		optionTypeDto.setCdOption(optionType.getCdOption());
		return optionTypeDto;
	}
	
	/**
	 * Convert an option type dto to a option type entity
	 * @param optionTypeDto
	 * @return converted option type entity
	 */
	public static OptionType toBIZ(OptionTypeDto optionTypeDto) {
		OptionType optionType = new OptionType();
		optionType.setId(optionTypeDto.getId());
		optionType.setLabel(optionTypeDto.getLabel());
		optionType.setPrice(optionTypeDto.getPrice());
		optionType.setCdOption(optionTypeDto.getCdOption());
		return optionType;
	}
}
