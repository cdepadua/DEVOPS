package com.devops.devops.converter;

import com.devops.devops.dto.OptionForReservationDto;
import com.devops.devops.dto.RoomForReservationDto;
import com.devops.devops.model.Option;
import com.devops.devops.model.Room;

final public class OptionForReservationDtoConverter {
	private OptionForReservationDtoConverter(){}
	
	public static OptionForReservationDto toDTO(Option option) {
		OptionForReservationDto optionDto = new OptionForReservationDto();
		optionDto.setCdHotel(option.getHotel().getCdHotel());
		optionDto.setId(option.getId());
		optionDto.setType(option.getOptionType().getCdOption());
		optionDto.setPrice(option.getOptionType().getPrice());
		return optionDto;
	}
}
