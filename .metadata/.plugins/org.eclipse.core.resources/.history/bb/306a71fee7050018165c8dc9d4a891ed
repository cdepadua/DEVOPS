package com.devops.devops.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.devops.devops.converter.OptionTypeDtoConverter;
import com.devops.devops.dto.OptionTypeDto;
import com.devops.devops.service.IOptionService;

/**
 * @author cesar
 * Option controller
 */
@RestController
@RequestMapping("/api/devops/option")
public class OptionController {
	
	/** Option service. */
	@Autowired
	IOptionService optionService;
	
	/**
	 * Create a new option type
	 * @param optionTypeDto
	 * @return created option type dto
	 */
	@RequestMapping(value = "/adminauth/create_option_type", method = RequestMethod.POST)
	public final OptionTypeDto createOptionType (@RequestBody final OptionTypeDto optionTypeDto) {
		return OptionTypeDtoConverter.toDTO(this.optionService
				.createNewOptionType(OptionTypeDtoConverter.toBIZ(optionTypeDto)));
	}
}
