package com.devops.devops.converter;

import java.util.ArrayList;
import java.util.HashMap;

import com.devops.devops.dto.HotelDto;
import com.devops.devops.dto.OptionsDto;
import com.devops.devops.dto.RoomsDto;
import com.devops.devops.model.Hotel;
import com.devops.devops.model.Option;
import com.devops.devops.model.OptionType;
import com.devops.devops.model.Room;
import com.devops.devops.model.RoomType;

/**
 * @author cesar
 * Hotel Dto converter
 */
public final class HotelDtoConverter {
	
	/** Constructor. */
	private HotelDtoConverter(){}
	
	/**
	 * Convert a hotel entity in hotel dto
	 * @param hotel
	 * @return converted dto
	 */
	public static HotelDto toDTO(final Hotel hotel){
		HotelDto hotelDto = new HotelDto();
		hotelDto.setId(hotel.getId());
		hotelDto.setName(hotel.getName());
		hotelDto.setCdHotel(hotel.getCdHotel());
		hotelDto.setAddress(hotel.getAddress());
		hotelDto.setEmail(hotel.getEmail());
		hotelDto.setPhoneNumber(hotel.getPhoneNumber());
		ArrayList<RoomsDto> roomsDto = new ArrayList<>();
		hotel.getRooms().forEach(room ->{
			RoomsDto roomDto = new RoomsDto();
			roomDto.setId(room.getId());
			roomDto.setNbOfRoom(room.getNumber());
			roomDto.setType(room.getRoomType().getCdRoom());
			roomsDto.add(roomDto);
		});
		hotelDto.setRooms(roomsDto);
		ArrayList<OptionsDto> optionsList = new ArrayList<>();
		hotel.getOptions().forEach(option -> {
			OptionsDto optionDto = new OptionsDto();
			optionDto.setId(option.getId());
			optionDto.setNumber(option.getNumber());
			optionDto.setOptionName(option.getOptionType().getCdOption());
			optionsList.add(optionDto);
		});
		hotelDto.setOptions(optionsList);
		return hotelDto;
	}
	
	/** Convert hotel dto into hotel entity
	 * @param hotelDto hotel dto to convert
	 * @param roomsType list of rooms type
	 * @param optionsType list of option type
	 * @return converted hotel entity
	 */
	public static Hotel toBIZ(final HotelDto hotelDto, HashMap<String, RoomType> roomsType, HashMap<String, OptionType> optionsType){
		Hotel hotel = new Hotel();
		hotel.setId(hotelDto.getId());
		hotel.setName(hotelDto.getName());
		hotel.setCdHotel(hotelDto.getCdHotel());
		hotel.setAddress(hotelDto.getAddress());
		hotel.setPhoneNumber(hotelDto.getPhoneNumber());
		hotel.setEmail(hotelDto.getEmail());
		ArrayList<Room> roomsList = new ArrayList<>();
		hotelDto.getRooms().forEach(roomDto -> {
			Room room = new Room();
			room.setId(roomDto.getId());
			room.setNumber(roomDto.getNbOfRoom());
			room.setRoomType(roomsType.get(roomDto.getType()));
			room.setHotel(hotel);
			roomsList.add(room);
		});
		hotel.setRooms(roomsList);
		ArrayList<Option> optionsList = new ArrayList<>();
		hotelDto.getOptions().forEach(optionDto -> {
			Option option = new Option();
			option.setId(optionDto.getId());
			option.setNumber(optionDto.getNumber());
			option.setOptionType(optionsType.get(optionDto.getOptionName()));
			option.setHotel(hotel);
			optionsList.add(option);
		});
		hotel.setOptions(optionsList);
		return hotel;
	}
}
