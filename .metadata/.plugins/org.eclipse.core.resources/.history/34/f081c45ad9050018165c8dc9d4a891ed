package com.devops.devops.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author cesar
 * Option reservation entity
 */
@Entity
@Table(name = "option_reservation")
@EntityListeners(AuditingEntityListener.class)
public class OptionReservation implements Serializable{

	/**
	 * Default serial uid 
	 */
	private static final long serialVersionUID = 1L
	
	/** Rooms reservation Id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/** Reserved room. */
	@NotNull
	@ManyToOne (cascade=CascadeType.ALL)
	private Option option;
	
	/** Referred reservation. */
	@NotNull
	@ManyToOne(cascade=CascadeType.ALL)
	private Reservation reservation;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the option
	 */
	public Option getOption() {
		return option;
	}

	/**
	 * @param option the option to set
	 */
	public void setOption(Option option) {
		this.option = option;
	}

	/**
	 * @return the reservation
	 */
	public Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation the reservation to set
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	

}
