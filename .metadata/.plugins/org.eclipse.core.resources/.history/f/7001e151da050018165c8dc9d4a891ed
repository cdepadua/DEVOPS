package com.devops.devops.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author cesar
 * Room reservation entity
 */
@Entity
@Table (name = "rooms_reservation")
@EntityListeners(AuditingEntityListener.class)
public class RoomReservation implements Serializable {

	/**
	 * Default UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/** Rooms reservation Id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	/** Reserved room. */
	@NotNull
	@ManyToOne (cascade=CascadeType.ALL)
	private Room room;
	
	/** Referred reservation. */
	@NotNull
	@ManyToOne(cascade=CascadeType.ALL)
	private Reservation reservation;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the room
	 */
	public Room getRoom() {
		return room;
	}

	/**
	 * @param room the room to set
	 */
	public void setRoom(Room room) {
		this.room = room;
	}

	/**
	 * @return the reservation
	 */
	public Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation the reservation to set
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
}
